import { AppComputer } from './computer.js'

// DOM ELEMENTS
document.getElementById("payLoan").style.visibility="hidden";

//BANK OBJECT
window.bank = {
    bankBalance : 0,
    loan : 0,
    activeLoan : false,
    computerConst : false,
    
    getLoan() { //Take up loan function
        if (this.activeLoan == false && this.computerConst == false) {
        let maxLoanAm = this.bankBalance * 2
        let loanAm = prompt("How much $$ do you want to loan?", "Write amount here")
        loanAm = parseInt(loanAm)
        if (loanAm < maxLoanAm && loanAm != null ) {
            let loan = loanAm
            this.bankBalance += loanAm
            document.getElementById("bBalance").innerHTML = "Balance: "+ bank.bankBalance +"kr";
            document.getElementById("outStndLoan").innerHTML = "Outstanding loan: " +loan +"kr"
            document.getElementById("payLoan").style.visibility="visible";
            this.activeLoan = true; 
            this.loan = loan
            this.computerConst = true
        }
        else {
            alert ("Invalid amount, bank can only loan you 2x your current balance")
        }
        } else {alert ("you already have loan, pls repay ur current loan b4 getting new 1, or u haven't baught a new computer with ur previous loan")}
    },
    
    payLoan() { // Pay loan function
        this.loan -= work.payBalance
        if (this.loan >= 0){ // if there is more loan left
            document.getElementById("outStndLoan").innerHTML = "Outstanding loan: " +this.loan +"kr"
            work.payBalance = 0
            document.getElementById("pBalance").innerHTML = 0;
        }
        else { // if there is no loan left to pay
            work.payBalance =- this.loan 
            this.loan = 0
            document.getElementById("outStndLoan").innerHTML = "Outstanding loan: " +0
            document.getElementById("pBalance").innerHTML = work.payBalance;
            this.activeLoan = false
        }

    },

    buyComp() { // Buy computer function
        const oPrice = document.getElementById("pcPrice").innerText;
        const price = parseInt(oPrice)
        if (price <= this.bankBalance) {
            this.bankBalance -= price
            document.getElementById("bBalance").innerHTML = "Balance: "+ bank.bankBalance +"kr";
            alert("Congratulations you are the proud owneer of a new computer")
            this.computerConst = false

        }
        else {

            alert("You need more money in ur bank balance to buy this computer")
        }

    }
}

//WORK/PERSON OBJECT
window.work = {
    payBalance : 0,
    pay : 100,
    
    working() { // Pay function
        this.payBalance = this.payBalance + this.pay
        document.getElementById("pBalance").innerHTML = this.payBalance;
    },
    
    banking(bank) { // BANK function
        if(bank.activeLoan == false) { //if no loan 
            bank.bankBalance += this.payBalance 
            document.getElementById("bBalance").innerHTML = "Balance: "+ bank.bankBalance +"kr";
            
        } else { // else loan active
            console.log(bank.loan)
            bank.bankBalance += (this.payBalance * 0.9)
            bank.loan -= (this.payBalance * 0.1)
            console.log(bank.loan)
            document.getElementById("bBalance").innerHTML = "Balance: "+ bank.bankBalance +"kr";
            document.getElementById("outStndLoan").innerHTML = "Outstanding loan: " + bank.loan

        }
        this.payBalance = 0
        document.getElementById("pBalance").innerHTML = this.payBalance;
    }
}

class App {
    constructor() {
        this.elStatus = document.getElementById('app-status')
        this.computer = new AppComputer()
    }

    async init() {
        await this.computer.init()
        this.render()
    }

    render() {
        this.computer.render()
    }
}

new App().init()



