const BASE_URL = "http://localhost:3000"

export function fetchComputers() {
    return fetch (`${ BASE_URL}/computers`)
        .then(response => response.json())
}

export function fetchComputerByID (id){
    return fetch (`${ BASE_URL}/computers/${id}`)
        .then(extractJsonFromBody)
}