export class AppComputerInformation {

    elComputerInformation = document.getElementById('secDisplay')
    computer;

    setComputer(computer) {
        this.computer = computer
        this.render()
    }

    render() {

        if (!this.computer) {
            return;
        }
        
        // NAME
        const pcName = document.getElementById("pcName")
        pcName.innerText = this.computer.name
        
        //PRICE
        const pcPrice = document.getElementById("pcPrice")
        pcPrice.innerText = `${this.computer.price} kr.`

        // DESCRIPTION
        const pcDesc = document.getElementById("pcDesc")
        pcDesc.innerText = this.computer.description

        // IMAGE
        const pcImage = document.getElementById("pcImage")
        pcImage.innerHTML = ""
        const image = new Image()
        image.width = 90
        image.onload = () => {
            pcImage.appendChild(image)
        }
        image.src = this.computer.imgUrl
      
    }
}
